#include "sb_display.h"

SbDisplay::SbDisplay(int bellButtonPin,
    int displayDcPin, int displayCsPin, int displayResetPin, int displayAutoOffDelay,
    int proxMinDistance, int proxMaxDistance, int proxEchoPin, int proxTrigPin) {
    bellButton = new PushButton(bellButtonPin);
    proxSensor = new ProximitySensor(proxMinDistance, proxMaxDistance, proxEchoPin, proxTrigPin);
    display = new OledDisplay(displayDcPin, displayCsPin, displayResetPin, displayAutoOffDelay);
}

void SbDisplay::init(void) {
    bellButton->init();
    proxSensor->init();
    display->init();

    Particle.subscribe("from_bell", &SbDisplay::bellEventDispatcher, this, MY_DEVICES);
    Webhook::subscribe("get_bell_data", &SbDisplay::getBellData, this);
    Particle.function("from_db", &SbDisplay::dbEventDispatcher, this);
    Particle.function("from_db_msg", &SbDisplay::dbMessageEventDispatcher, this); // custom message sent from mobile and relayed by server

    triggerDataUpdate();
}

void SbDisplay::loop(void) {
    if (proxSensor->proximityCheck()) {
        display->print(data.welcome_message, true);
    }

    if (bellButton->checkPressed()) {
        Webhook::publish("call_button_pressed", {{"backand_anonymous_token", BACKAND_ANONYMOUS_TOKEN}, {"ring_on", currentTimeString()}, {"bell_id", data.id}});
    }
}

void SbDisplay::getBellData(const char *event, const char *dataString) {
    data.populate(dataString);
}

int SbDisplay::dbMessageEventDispatcher(String message) {
    eventDispatcher("print_custom_message", message);
}

int SbDisplay::dbEventDispatcher(String eventName) {
    eventDispatcher(eventName, "");
    return 0;
}

void SbDisplay::bellEventDispatcher(const char *particleEvent, const char *sbEvent) {
    String event = String(sbEvent);
    eventDispatcher(event, "");
}

int SbDisplay::eventDispatcher(String event, String payload) {
    if (event.equals("update_data")) {
        triggerDataUpdate();
    }

    if (event.equals("print_coming_msg")) {
        display->stopAutoOffTimer(); // Not to start twice if already running (proximity)
        display->print(data.coming_message, true);
    }

    if (event.equals("print_ringing_msg")) {
        display->stopAutoOffTimer(); // Not to start twice if already running (proximity)
        display->print("Ringing!", true);
    }

    if (event.equals("print_come_up_msg")) {
        display->stopAutoOffTimer(); // Not to start twice if already running (proximity)
        display->print(data.come_up_message, true);
    }

    if (event.equals("print_no_answer_msg")) {
        display->stopAutoOffTimer(); // Not to start twice if already running (proximity)
        display->print(data.no_answer_message, true);
    }

    if (event.equals("print_custom_message")) {
        display->stopAutoOffTimer(); // Not to start twice if already running (proximity)
        display->print(payload, true);
    }

}

void SbDisplay::triggerDataUpdate(void) {
    Webhook::publish("get_bell_data", {{"backand_anonymous_token", BACKAND_ANONYMOUS_TOKEN}, {"device_type", "particle_display_id"}, {"device_id", System.deviceID()}});
}

String SbDisplay::currentTimeString(void) {
    time_t  time = Time.now();
    String  timeString(Time.format(time, TIME_FORMAT_ISO8601_FULL));

    return timeString;
}
