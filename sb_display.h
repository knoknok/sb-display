#ifndef _SB_DISPLAY_H_
#define _SB_DISPLAY_H_

#include "application.h"
#include "backand.h"
#include "webhook.h"
#include "push_button.h"
#include "proximity_sensor.h"
#include "oled_display.h"
#include "sb_data.h"

class SbDisplay {
    public:
        SbData          data;

        PushButton      *bellButton;
        OledDisplay     *display;
        ProximitySensor *proxSensor;

        SbDisplay(int bellButtonPin,
        int     displayDcPin, int displayCsPin, int displayResetPin, int displayAutoOffDelay,
        int     proxMinDistance, int proxMaxDistance, int proxEchoPin, int proxTrigPin);
        void    init(void);
        void    loop(void);
        void    getBellData(const char *event, const char *dataString);
        int     eventDispatcher(String event, String payload);
        void    bellEventDispatcher(const char *particleEvent, const char *sbEvent);
        int     dbEventDispatcher(String eventName);
        int     dbMessageEventDispatcher(String message);
        void    triggerDataUpdate(void);
        String  currentTimeString(void);
};

#endif
