/***********/
/* DISPLAY */
/***********/

#include "sb_display.h"

#define DISPLAY_AUTO_OFF_DURATION   3000
#define PROX_MIN_DISTANCE           10
#define PROX_MAX_DISTANCE           35

SbDisplay   display(D2,
                    D3, D4, D5, DISPLAY_AUTO_OFF_DURATION,
                    PROX_MIN_DISTANCE, PROX_MAX_DISTANCE, D6, D1);

void setup() {
    Serial.begin(9600);
    display.init();
}

void loop() {
  display.loop();
}
